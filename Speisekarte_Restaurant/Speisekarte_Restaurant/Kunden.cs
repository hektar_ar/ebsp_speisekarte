﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Speisekarte_Restaurant
{
    public class Kunden
    {
        /// <summary>
        /// Gibt an, wie lange ein Kunde noch bereit ist, in der Warteschlange zu stehen.
        /// </summary>
        public int WartetNoch { get; set; }

        /// <summary>
        /// Gibt an, wie viele Personen sich in dieser Gruppe aufhalten.
        /// </summary>
        /// <remarks>Normalerweise 2-5</remarks>
        public int AnzahlPersonen { get; }

        /// <summary>
        /// Standardkonstruktor; die Gruppe enthält keine Kunden und hat eine Wartezeit von 0 Minuten.
        /// </summary>
        public Kunden()
        { }

        /// <summary>
        /// Eine Kundengruppe mit bestimmter Anzahl und Wartezeit.
        /// </summary>
        /// <param name="wartet">Gibt an, wie lange ein Kunde noch bereit ist, in der Warteschlange zu stehen.</param>
        /// <param name="anzahl">Gibt an, wie viele Personen sich in dieser Gruppe aufhalten.</param>
        public Kunden(int wartet, int anzahl)
        {
            WartetNoch = wartet;
            AnzahlPersonen = anzahl;
        }

        public override string ToString()
        {
            return "Anzahl: " + AnzahlPersonen + "; WartetNoch: " + WartetNoch;
        }
    }
}
