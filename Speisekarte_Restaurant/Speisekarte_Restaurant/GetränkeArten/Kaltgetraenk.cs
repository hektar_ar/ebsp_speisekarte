﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Speisekarte_Restaurant
{
    public class Kaltgetraenk : Getraenk
    {
        public Kaltgetraenk(string bez, float preis) : base(bez, preis)
        { }
    }
}
