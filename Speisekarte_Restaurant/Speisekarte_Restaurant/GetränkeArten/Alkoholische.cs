﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Speisekarte_Restaurant
{
    public class Alkoholische : Getraenk
    {
        public Alkoholische(string bez, float preis) : base(bez, preis)
        { }
    }
}
