﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Speisekarte_Restaurant
{
    public class AbrechnungSortierer : IComparer<Getraenk>
    {
        private AbrechnungSortierart art;

        public AbrechnungSortierer(AbrechnungSortierart art)
        {
            this.art = art;
        }

        public int Compare(Getraenk x, Getraenk y)
        {
            switch (art)
            {
                case AbrechnungSortierart.AnzahlVerkauft:
                    return -x.AnzahlBestellt.CompareTo(y.AnzahlBestellt);
                case AbrechnungSortierart.Gewinn:
                    return -((x.Preis * x.AnzahlBestellt).CompareTo(y.Preis * y.AnzahlBestellt));
                default:
                    throw new ArgumentException("Invalid value.", nameof(art));
            }
        }
    }

    /// <summary>
    /// Gibt die Art an, nach der sortiert werden soll.
    /// Es wird immer absteigend sortiert.
    /// </summary>
    public enum AbrechnungSortierart
    {
        AnzahlVerkauft, Gewinn
    }
}
