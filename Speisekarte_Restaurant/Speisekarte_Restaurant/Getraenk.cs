﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Speisekarte_Restaurant
{
    /// <summary>
    /// Stellt die Grundlage für alle Getränkkategorien dar.
    /// </summary>
    public abstract class Getraenk
    {
        /// <summary>
        /// Der Name des Getränks.
        /// </summary>
        public string Bezeichnung { get; }

        /// <summary>
        /// Der Preis der Getränks.
        /// </summary>
        public float Preis { get; }

        /// <summary>
        /// Wie oft das Getränk bis jetzt bestellt wurde.
        /// </summary>
        public int AnzahlBestellt { get; set; }

        public Getraenk(string bez, float preis)
        {
            Bezeichnung = bez;
            Preis = preis;
        }
    }
}
