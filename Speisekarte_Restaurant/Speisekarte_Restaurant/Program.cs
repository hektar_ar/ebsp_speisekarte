﻿// Gruppe: Scheifinger, Schwarz, Klement, Götz
// Eigenbeispiel: Speisekarte und Restaurantsimulation
// Angabe: angabe.docx
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Speisekarte_Restaurant
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var sk = new Speisekarte("demo.csv");
            //var sim = new Simulation(new Speisekarte(new List<Getraenk> { new Kaffee("kaf", 1f), new Tee("tee", 1.5f) }));
            var sim = new Simulation(sk, 5);

            sim.Durchfuehren();

            sim.Abrechnung();

            Console.ReadKey();
        }
    }
}
