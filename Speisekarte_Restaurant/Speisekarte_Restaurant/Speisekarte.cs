﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Speisekarte_Restaurant
{
    public class Speisekarte
    {
        public List<Getraenk> Getraenke { get; }

        public Speisekarte(List<Getraenk> list)
        {
            Getraenke = list;
        }

        public Speisekarte(string path)
        {
            Getraenke = new List<Getraenk>();
            var sr = new StreamReader(path, Encoding.UTF7);

            while (!sr.EndOfStream)
            {
                string[] zeile = sr.ReadLine().Split(';');

                switch (zeile[0].ToLower())
                {
                    case "alkoholische":
                        Getraenke.Add(new Alkoholische(zeile[1], float.Parse(zeile[2])));
                        break;
                    case "kaffee":
                        Getraenke.Add(new Kaffee(zeile[1], float.Parse(zeile[2])));
                        break;
                    case "kaltgetraenk":
                        Getraenke.Add(new Kaltgetraenk(zeile[1], float.Parse(zeile[2])));
                        break;
                    case "tee":
                        Getraenke.Add(new Tee(zeile[1], float.Parse(zeile[2])));
                        break;
                    default:
                        throw new InvalidDataException("Ungültige Kategorie: " + zeile[0]);
                }
            }

            Getraenke.Sort(new SpeisekarteSortierer());
        }
    }
}
