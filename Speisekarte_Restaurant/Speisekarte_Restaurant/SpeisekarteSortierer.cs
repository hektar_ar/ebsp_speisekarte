﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Speisekarte_Restaurant
{
    public class SpeisekarteSortierer : IComparer<Getraenk>
    {
        public int Compare(Getraenk x, Getraenk y)
        {
            if (x.GetType() == y.GetType())
                return x.Preis.CompareTo(y.Preis);

            return        ((SpeisekarteReihenfolge)Enum.Parse(typeof(SpeisekarteReihenfolge), x.GetType().Name))
                .CompareTo((SpeisekarteReihenfolge)Enum.Parse(typeof(SpeisekarteReihenfolge), y.GetType().Name));
        }

        /// <summary>
        /// Reihenfolge, in der die Getränkearten sortiert werden sollen.
        /// </summary>
        /// <remarks>Muss genauso benannt sein, wie die jeweilige Klasse</remarks>
        private enum SpeisekarteReihenfolge
        {
            Kaffee, Tee, Alkoholische, Kaltgetraenk
        }
    }
}
