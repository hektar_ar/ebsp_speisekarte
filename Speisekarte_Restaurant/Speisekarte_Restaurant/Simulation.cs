﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Speisekarte_Restaurant
{
    public class Simulation
    {
        private static Random rnd = new Random();
        private List<Tisch> tische;
        private List<Kunden> wartend;
        private Speisekarte karte;
        private int zeit = 0, nextTischFreiZeit = 30, sumGaeste = 0, anzGruppen = 0;
        private float sumPreise = 0;

        /// <summary>
        /// Erstellt eine neue Simulation mit angegebener Speisekarte.
        /// </summary>
        /// <param name="karte">Die gewünschte Speisekarte</param>
        /// <param name="anzahlTische">Die Anzahl der Tische</param>
        public Simulation(Speisekarte karte, int anzahlTische = 5)
        {
            tische = new List<Tisch>();
            wartend = new List<Kunden>();
            this.karte = karte;

            while (anzahlTische-- > 0)
                tische.Add(new Tisch());
        }

        /// <summary>
        /// Führt die Simulation für die angegebene Zeitspanne durch.
        /// </summary>
        /// <param name="laufzeit">Die Zeitspanne in Minuten</param>
        public void Durchfuehren(int laufzeit = 60 * 5)
        {
            bool ende = false; // es kommen keine neuen Kunden mehr
            while (zeit < laufzeit || (ende = tische.Any(t => t.Besetzt)))
                MinuteSimulieren(ende);
        }

        /// <summary>
        /// Simuliert das Geschehen für eine Minute.
        /// </summary>
        private void MinuteSimulieren(bool ende)
        {
            // Alle 10 Minuten; falls noch Gäste kommen; 50 %ige Chance
            if (zeit % 10 == 0 && !ende && rnd.Next(2) == 0)
            {
                // Gruppe von 2-5 Personen
                int anz = rnd.Next(2, 6);
                sumGaeste += anz;
                ++anzGruppen;
                
                // Falls kein Tisch frei ist wartet die Gruppe 2-15 Minuten
                var kunden = new Kunden(rnd.Next(2, 16), anz);

                // Falls ein Tisch frei ist
                if (tische.Any(t => !t.Besetzt))
                {
                    tische.First(t => !t.Besetzt).Besetzt = true;
                    KundenBestellen(kunden);
                }
                else
                    wartend.Add(kunden);
            }

            // Nach 20-30 Minuten wird der nächste Tisch frei
            if (nextTischFreiZeit <= 0)
            {
                nextTischFreiZeit = rnd.Next(20, 31);

                // Falls mindestens ein Tisch besetzt ist
                if (tische.Any(t => t.Besetzt))
                {
                    // Falls jemand gerade wartet
                    if (wartend.Any())
                        // Nächste Gruppe gleich zu Tisch führen
                        // (leicht unrealistisch, weil es keine Aufräumzeit gibt)
                    {
                        Kunden k = wartend.First();
                        wartend.Remove(k);
                        KundenBestellen(k);
                    }
                    else
                        // Tisch frei machen
                        tische.First(t => t.Besetzt).Besetzt = false;
                        // .First kann hier benutzt werden, weil definitiv mindestens einer vorhanden sein muss
                }
            }

            // Jede Gruppe, die wartet...
            for (int i = 0; i < wartend.Count; ++i)
            {
                // ...verschwindet, wenn die Zeit abgelaufen ist
                if (--wartend[i].WartetNoch <= 0)
                    wartend.RemoveAt(i--);
            }

            ++zeit;
            --nextTischFreiZeit;
        }

        /// <summary>
        /// Wählt ein zufälliges Getränk aus <see cref="karte"/>.
        /// </summary>
        /// <returns>Zufällig ausgewähltes <see cref="Getraenk"/></returns>
        private Getraenk GetRandomGetraenk() => karte.Getraenke[rnd.Next(karte.Getraenke.Count)];
        
        /// <summary>
        /// Jeder Kunde bestellt ein zufällig ausgewähltes Getränk.
        /// </summary>
        /// <param name="kunden">Die Kundengruppe</param>
        private void KundenBestellen(Kunden kunden)
        {
            // Jeder Gast bestellt zufällig ein Getränk
            for (int i = 0; i < kunden.AnzahlPersonen; ++i)
            {
                Getraenk g = GetRandomGetraenk();
                // Für die Abrechnung
                sumPreise += g.Preis;
                ++g.AnzahlBestellt;
            }
        }

        /// <summary>
        /// Gibt die Abrechnung des Tages in der Konsole aus.
        /// </summary>
        public void Abrechnung()
        {
            int maxLen = karte.Getraenke.Max(g => g.Bezeichnung.Length) + 1;
            Console.WriteLine("Öffnungszeit: " + zeit + " Minuten");
            Console.WriteLine("Anzahl der heutigen Gäste: " + sumGaeste + " (In " + anzGruppen + " Gruppen)");
            Console.WriteLine("Heutige Einnahmen: EUR " + sumPreise.ToString("f"));

            Console.WriteLine();

            Console.WriteLine("Sortiert nach Anzahl der Verkäufe:");
            karte.Getraenke.Sort(new AbrechnungSortierer(AbrechnungSortierart.AnzahlVerkauft));
            foreach (var g in karte.Getraenke)
                Console.WriteLine((g.Bezeichnung + ":").PadRight(maxLen) + g.AnzahlBestellt);

            Console.WriteLine();
            
            Console.WriteLine("Sortiert nach erwirtschaftetem Gewinn:");
            karte.Getraenke.Sort(new AbrechnungSortierer(AbrechnungSortierart.Gewinn));
            foreach (var g in karte.Getraenke)
                Console.WriteLine((g.Bezeichnung + ":").PadRight(maxLen) + g.AnzahlBestellt + " * " + g.Preis.ToString("f") + " = " + (g.AnzahlBestellt * g.Preis).ToString("f"));
        }
    }
}
